package org.deroesch.deployer.dcases;

import static org.junit.jupiter.api.Assertions.*;

import org.deroesch.deployer.entities.Fields;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AppEngineCaseTest {

	AppEngineCase dc;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		dc = (AppEngineCase) DCase.create(DCase.AppEngine);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAppEngineCase() {
		assertEquals(DCase.AppEngine, new AppEngineCase().getCase());
	}

	@Test
	void testAppEngineCaseWithArgs() {
		dc = new AppEngineCase("name", "orgID", "family", "app");
		assertEquals("name", dc.getName());
		assertEquals("orgID", dc.getOrgID());
		assertEquals("family", dc.getFamily());
		assertEquals("app", dc.getApp());

	}

	@Test
	void testGetKey() {
		String v = Fields.nextString();
		dc.setKey(v);
		assertEquals(v, dc.getKey());
	}

	@Test
	void testGetName() {
		String v = Fields.nextString();
		dc.setName(v);
		assertEquals(v, dc.getName());
	}

	@Test
	void testGetOrgID() {
		String v = Fields.nextString();
		dc.setOrgID(v);
		assertEquals(v, dc.getOrgID());
	}

	@Test
	void testGetFamily() {
		String v = Fields.nextString();
		dc.setFamily(v);
		assertEquals(v, dc.getFamily());
	}

	@Test
	void testGetApp() {
		String v = Fields.nextString();
		dc.setApp(v);
		assertEquals(v, dc.getApp());
	}

	@Test
	void testGetCase() {
		assertEquals(DCase.AppEngine, dc.getCase());
	}

	@Test
	void testToString() {
		assertTrue(dc.toString().startsWith("AppEngineCase"));
	}

}
