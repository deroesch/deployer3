package org.deroesch.deployer.dcases;

import static org.junit.jupiter.api.Assertions.*;

import org.deroesch.deployer.dcases.DCase;
import org.deroesch.deployer.dcases.IDCase;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DCaseTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreate() throws ExBadArgs {
		IDCase f = DCase.create(DCase.AppEngine);
		assertEquals(DCase.AppEngine, f.getCase());

		f = DCase.create(DCase.Nexus);
		assertEquals(DCase.Nexus, f.getCase());

		f = DCase.create(DCase.VM);
		assertEquals(DCase.VM, f.getCase());

		f = DCase.create(DCase.Wizard);
		assertEquals(DCase.Wizard, f.getCase());

		// Test bad DCase type
		try {
			DCase.create(DCase.None);
			fail("Allowed DCase.None in DCaseTest.testCreate()");
		} catch (ExBadArgs e) {
			// Expected, ignore
		}

		// Test null
		try {
			DCase.create(null);
			fail("Allowed null in DCaseTest.testCreate()");
		} catch (AssertionError e) {
			// Expected, ignore
		}
	}
}