package org.deroesch.deployer.services;

import static org.junit.jupiter.api.Assertions.*;

import org.deroesch.deployer.entities.SessionStage;
import org.deroesch.deployer.entities.SessionState;
import org.deroesch.deployer.entities.Session;
import org.deroesch.deployer.entities.Request;
import org.deroesch.deployer.entities.Role;
import org.deroesch.deployer.entities.User;
import org.deroesch.deployer.services.SessionService;
import org.deroesch.deployer.services.SessionServiceImpl;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.deployer.services.ex.ExInvalidState;
import org.deroesch.deployer.services.ex.ExUnauthorizedUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SessionServiceTest {

	private static SessionService sessionService;
	private static User user;
	private static Session session;
	private static String uName = "testUser";
	private static Role allRoles[] = Role.AppCreator.getDeclaringClass().getEnumConstants();

	@BeforeEach
	void setUp() throws Exception {

		// TODO Get rid of this
		final Request request = new Request();

		sessionService = new SessionServiceImpl();
		user = new User(uName);
		session = new Session(request, user.getUname());
	}

	/**
	 * Test straight-through-processing. We give the test user the permission to do
	 * everything. This test looks at the state sequence.
	 * 
	 * @throws ExUnauthorizedUser
	 * @throws ExInvalidState
	 */
	@Test
	void testAll() throws ExUnauthorizedUser, ExInvalidState {
		user.addRole(Role.User);
		user.addRole(Role.SessionCreator);
		user.addRole(Role.Approver);
		user.addRole(Role.Executor);

		sessionService.edit(session, user);
		assertEquals(uName, session.getEditedBy());

		sessionService.submit(session, user);
		assertEquals(uName, session.getSubmittedBy());
		sessionService.recall(session, user);

		sessionService.submit(session, user);
		assertEquals(uName, session.getSubmittedBy());
		sessionService.reject(session, user);

		sessionService.submit(session, user);
		assertEquals(uName, session.getSubmittedBy());
		sessionService.approve(session, user);
		assertEquals(uName, session.getApprovedBy());

		sessionService.execute(session, user);
		assertEquals(uName, session.getExecutedBy());
	}

	@Test
	void testCreate() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		user.addRole(Role.SessionCreator);
		session = sessionService.create(user, null);
		assertEquals(SessionState.CREATED, session.getState());
	}

	@Test
	void testEdit() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.SessionCreator, SessionState.CREATED, SessionState.CREATED, (service, session, user) -> {
			service.edit(session, user);
		});
	}

	@Test
	void testSaveAndValidate() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.SessionCreator, SessionState.CREATED, SessionState.CREATED, (service, session, user) -> {
			service.saveAndValidate(session, user);
		});
	}

	@Test
	void testDelete() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.SessionCreator, SessionState.CREATED, SessionState.DELETED, (service, session, user) -> {
			service.delete(session, user);
		});
	}

	@Test
	void testSubmit() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.SessionCreator, SessionState.CREATED, SessionState.SUBMITTED, (service, session, user) -> {
			service.submit(session, user);
		});
	}

	@Test
	void testRecall() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.SessionCreator, SessionState.SUBMITTED, SessionState.CREATED, (service, session, user) -> {
			service.recall(session, user);
		});
	}

	@Test
	void testReject() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.Approver, SessionState.SUBMITTED, SessionState.CREATED, (service, session, user) -> {
			service.reject(session, user);
		});
	}

	@Test
	void testApprove() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.Approver, SessionState.SUBMITTED, SessionState.APPROVED, (service, session, user) -> {
			service.approve(session, user);
		});
	}

	@Test
	void testExecute() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.Executor, SessionState.APPROVED, SessionState.PENDING, (service, session, user) -> {
			service.execute(session, user);
		});
	}

	@Test
	void testCancel() throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {
		testOp(Role.Approver, SessionState.APPROVED, SessionState.CANCELED, (service, session, user) -> {
			service.cancel(session, user);
		});
	}

	/*
	 * Helpers
	 */

	/*
	 * Call the a session operation r.something(s, u); as a functor.
	 */
	interface Op {
		void apply(SessionService r, Session s, User u) throws ExUnauthorizedUser, ExInvalidState, ExBadArgs;
	}

	/**
	 * Validate state changes for every call.
	 * 
	 * @param operator      The command to execute
	 * @param requiredRole  The Role the user must have to make this call
	 * @param requiredState The state this session MUST be in BEFORE making the call
	 * @param expectedState The state this session MUST be in AFTER making the call
	 * @throws ExUnauthorizedUser
	 * @throws ExInvalidState
	 * @throws ExBadArgs
	 */
	void testOp(Role requiredRole, SessionState requiredState, SessionState expectedState, Op operator)
			throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {

		// Give user permission to call this command
		user.addRole(requiredRole);

		// Try a correct call
		session.setState(requiredState);
		operator.apply(sessionService, session, user);
		assertEquals(expectedState, session.getState());

		// Make sure all the other Roles throw ExUnauthorizedAccess
		session.setState(requiredState);
		for (Role testRole : allRoles) {

			// Skip the correct case!
			if (testRole.equals(requiredRole))
				continue;

			// All other roles should throw
			try {
				User unauthorizedUser = new User("user");
				unauthorizedUser.addRole(testRole);

				operator.apply(sessionService, session, unauthorizedUser);
				fail("Blocked a good user role: " + testRole);
			} catch (ExUnauthorizedUser e) {
				// Expected
			}
		}

	}

	@Test
	void testRequireState() throws ExUnauthorizedUser {

		// TODO Get rid of this
		final Request request = new Request();

		try {
			user.addRole(Role.Approver);
			session.setState(SessionState.DELETED);
			session = new Session(request, user.getUname());
			sessionService.reject(session, user);
			fail("Allowed bad state for delete()");
		} catch (ExUnauthorizedUser e) {
			throw e;
		} catch (ExInvalidState e) {
			// Expected, ignore.
		}
	}

	@Test
	void testRequireStage() throws ExUnauthorizedUser, ExInvalidState {

		// TODO Get rid of this
		final Request request = new Request();

		try {
			user.addRole(Role.Approver);
			session = new Session(request, user.getUname());
			session.setStage(SessionStage.FINISHED);
			sessionService.cancel(session, user);
			fail("Allowed bad stage for cancel()");
		} catch (ExUnauthorizedUser e) {
			throw e;
		} catch (ExInvalidState e) {
			// Expected, ignore.
		}
	}

}
