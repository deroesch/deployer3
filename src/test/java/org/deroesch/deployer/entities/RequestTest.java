package org.deroesch.deployer.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.deroesch.deployer.dcases.DCase;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RequestTest {
	private Request request;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		request = new Request();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testRequest() {
		Request r = new Request();
		assertEquals(SessionState.CREATED, r.getState());
	}

	@Test
	void testRequestWithArgs() {
		Request r = new Request("a", "n", "p", "api", "web", DCase.AppEngine);
		assertEquals("a", r.getArtifactID());
		assertEquals("n", r.getNodeID());
		assertEquals("p", r.getPipeline());
		assertEquals("api", r.getApiToken());
		assertEquals("web", r.getWebhookToken());
		assertEquals(DCase.AppEngine, r.getCase());
		assertEquals(SessionState.CREATED, r.getState());
	}

	@Test
	void testGetArtifactID() {
		String v = Fields.nextString();
		request.setArtifactID(v);
		assertEquals(v, request.getArtifactID());
	}

	@Test
	void testGetNodeID() {
		String v = Fields.nextString();
		request.setNodeID(v);
		assertEquals(v, request.getNodeID());
	}

	@Test
	void testGetPipeline() {
		String v = Fields.nextString();
		request.setPipeline(v);
		assertEquals(v, request.getPipeline());
	}

	@Test
	void testGetApiToken() {
		String v = Fields.nextString();
		request.setApiToken(v);
		assertEquals(v, request.getApiToken());
	}

	@Test
	void testGetWebhookToken() {
		String v = Fields.nextString();
		request.setWebhookToken(v);
		assertEquals(v, request.getWebhookToken());
	}

	@Test
	void testSetCase() {
		DCase f = DCase.AppEngine;
		request.setCase(f);
		assertEquals(f, request.getCase());
	}

	@Test
	void testSetKey() {
		String v = Fields.nextString();
		request.setKey(v);
		assertEquals(v, request.getKey());
	}

	@Test
	void testAddSession() throws ExBadArgs {
		// Session list is empty for this request
		assertEquals(0, request.getSessions().size());

		// Let's add a session
		Session session = new Session(request);

		// Was the correct session added?
		assertEquals(1, request.getSessions().size());
		assertEquals(session, request.getSessions().get(0));

	}

	@Test
	void testGetState() throws ExBadArgs {
		assertEquals(SessionState.CREATED, request.getState());

		// Let's add a session
		Session session = new Session(request);
		session.setState(SessionState.SUCCESS);

		// Check state
		assertEquals(SessionState.SUCCESS, request.getState());

		// Check state again.
		Session session2 = new Session(request);
		session2.setState(SessionState.FAILED);

		assertEquals(SessionState.FAILED, request.getState());
	}

	@Test
	void testToString() {
		assertTrue(request.toString().startsWith("DRequest"));
	}

}
