package org.deroesch.deployer.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserTest {

    // User under test
    User user;

    /**
     * Let's get a new User for each test.
     * 
     * @throws Exception
     */
    @BeforeEach
    void setUp() throws Exception {
        user = new User();
    }

    @Test
    void testUser() {
        user = new User();
        assertEquals(User.DEFAULT_UNAME, user.getUname());
        assertEquals(1, user.getRoles().size());
        assertTrue(user.hasRole(Role.User));
    }

    @Test
    void testUserString() {
        user = new User("user");
        assertEquals("user", user.getUname());
        assertEquals(1, user.getRoles().size());
        assertTrue(user.hasRole(Role.User));

        // Can't use null
        try {
            new User(null);
            fail("Allowed new User(null);");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore.
        }

        // Can't use ""
        try {
            new User("");
            fail("Allowed new User(<empty string>);");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore.
        }
    }

    @Test
    void testGetUname() {
        assertEquals(User.DEFAULT_UNAME, user.getUname());
    }

    @Test
    void testSetUname() {
        user.setUname("Hello");
        assertEquals("Hello", user.getUname());
        checkAssert(user -> {
            user.setUname(null);
        });
        checkAssert(user -> {
            user.setUname("");
        });
    }

    @Test
    void testGetRoles() {
        assertTrue(user.getRoles().size() > 0);
    }

    @Test
    void testSetRoles() {
        HashSet<Role> newRoles = new HashSet<Role>();
        user.setRoles(newRoles);
        assertTrue(newRoles == user.getRoles());
        checkAssert(user -> {
            user.setRoles(null);
        });
    }

    @Test
    void testHasRole() {
        assertTrue(user.hasRole(Role.User));
        checkAssert(user -> {
            user.hasRole(null);
        });
    }

    @Test
    void testAddRole() {
        assertFalse(user.hasRole(Role.Approver));
        user.addRole(Role.Approver);
        assertTrue(user.hasRole(Role.Approver));
        checkAssert(user -> {
            user.addRole(null);
        });
    }

    @Test
    void testRemoveRole() {
        assertFalse(user.hasRole(Role.Approver));
        user.addRole(Role.Approver);
        assertTrue(user.hasRole(Role.Approver));
        user.removeRole(Role.Approver);
        assertFalse(user.hasRole(Role.Approver));
    }

    @Test
    void testToString() {
        assertTrue(user.toString().startsWith("User ["));
    }

    /*
     * Check java assert keyword traps
     */

    @Test
    void checkCheckAssert() {
        final String newName = "Green";

        // CAN use good strings
        try {
            checkAssert(user -> {
                user.setUname(newName);
            });
            fail("Blocked a good string.");
        } catch (java.lang.AssertionError e) {
            // This should fail() in checkAssert();
        }

        // Make name was correctly set to "Green".
        assertEquals(newName, user.getUname());

        // CAN'T use nulls.
        checkAssert(user -> {
            user.setUname(null);
        });

        // CAN'T use empty strings
        checkAssert(user -> {
            user.setUname("");
        });

        // Make sure above 2 lambdas didn't change the name.
        assertEquals(newName, user.getUname());
    }

    // Lambda template to test an operations on a User
    interface Op {
        void apply(User u) throws java.lang.AssertionError;
    }

    /*
     * Catches the throw if a java assert keyword trips. Allows tests to continue.
     * The operator.apply(user) call should always throw.
     */
    void checkAssert(Op operator) {
        final String failMsg = "Allowed null or empty parameter.";
        try {
            operator.apply(user);
            fail(failMsg);
        } catch (java.lang.AssertionError e) {  // Looking for fails of java assert keyword here.

            // Rethrow of fail(failMsg), which also lands here...
            if (failMsg.equals(e.getMessage()))
                throw e;
        }
    }
}
