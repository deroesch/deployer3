package org.deroesch.deployer.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SessionTest {

	private Session session;
	private User user = new User();
	private Request request = new Request();

	@BeforeEach
	void setUp() throws Exception {
		session = new Session(request, user.getUname());
	}

	@Test
	void testSession() {
		session = new Session(request);
		assertEquals(User.DEFAULT_UNAME, session.getCreatedBy());
	}

	@Test
	void testSessionString() {
		session = new Session(request, "other");
		assertEquals("other", session.getCreatedBy());
	}

	@Test
	void testGetEditedBy() {
		String v = Fields.nextString();
		session.setEditedBy(v);
		assertEquals(v, session.getEditedBy());
	}

	@Test
	void testGetExecutedBy() {
		String v = Fields.nextString();
		session.setExecutedBy(v);
		assertEquals(v, session.getExecutedBy());
	}

	@Test
	void testGetApprovedBy() {
		String v = Fields.nextString();
		session.setApprovedBy(v);
		assertEquals(v, session.getApprovedBy());
	}

	@Test
	void testGetSubmittedBy() {
		String v = Fields.nextString();
		session.setSubmittedBy(v);
		assertEquals(v, session.getSubmittedBy());
	}

	@Test
	void testGetCanceledBy() {
		String v = Fields.nextString();
		session.setCanceledBy(v);
		assertEquals(v, session.getCanceledBy());
	}

	@Test
	void testGetCode() {
		String v = Fields.nextString();
		session.setCode(v);
		assertEquals(v, session.getCode());
	}

	@Test
	void testGetMessage() {
		String v = Fields.nextString();
		session.setMessage(v);
		assertEquals(v, session.getMessage());
	}

	@Test
	void testGetStage() {
		session.setStage(SessionStage.STARTED);
		assertEquals(SessionStage.STARTED, session.getStage());
	}

	@Test
	void testGetState() {
		session.setState(SessionState.APPROVED);
		assertEquals(SessionState.APPROVED, session.getState());

	}

	@Test
	void testGetCreateTime() {
		Date d = Fields.nextDate();
		session.setCreateTime(d);
		assertTrue(d == session.getCreateTime());
	}

	@Test
	void testGetEditTime() {
		Date d = Fields.nextDate();
		session.setEditTime(d);
		assertTrue(d == session.getEditTime());
	}

	@Test
	void testGetStartTime() {
		Date d = Fields.nextDate();
		session.setStartTime(d);
		assertTrue(d == session.getStartTime());
	}

	@Test
	void testGetFinishTime() {
		Date d = Fields.nextDate();
		session.setFinishTime(d);
		assertTrue(d == session.getFinishTime());
	}

	@Test
	void testGetApprovalTime() {
		Date d = Fields.nextDate();
		session.setApprovalTime(d);
		assertTrue(d == session.getApprovalTime());
	}

	@Test
	void testToString() {
		assertTrue(session.toString().startsWith("[DRequest ["));
		assertTrue(session.toString().contains("DSession"));
		assertTrue(session.toString().endsWith("]]"));
	} 

}
