package org.deroesch.deployer.entities;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Fields {

	private static AtomicInteger i = new AtomicInteger();
	private static AtomicLong j = new AtomicLong();

	public static String nextString() {
		return "Value-" + i.getAndIncrement();
	}

	public static Date nextDate() {
		return new Date(10000 * i.getAndIncrement());
	}

	public static int nextInt() {
		return i.getAndIncrement();
	}

	public static long nextLong() {
		return j.getAndIncrement();
	}

}
