/**
 * 
 */
package org.deroesch.deployer.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Date;

import org.deroesch.helpers.Utils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Canonical unit test
 */
class SampleTest {

    Sample sample;
    Date   date   = new Date();
    String string = Utils.NON_EMPTY_STRING;

    /**
     * @throws java.lang.Exception
     */
    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
        sample = new Sample(date, string);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
    }

    /**
     * Test method for {@link org.deroesch.deployer.entities.Sample#Sample()}.
     */
    @Test
    void testSample() {

        // All args null.
        sample = new Sample();

        // Check postconditions
        checkAssert(sample -> sample.getString());
        checkAssert(sample -> sample.getDate());

    }

    /**
     * Test method for
     * {@link org.deroesch.deployer.entities.Sample#Sample(java.util.Date, java.lang.String)}.
     */
    @Test
    void testSampleDateString() {
        Date   date   = new Date();
        String string = "Hello";

        Sample s = new Sample(date, string);

        assertEquals(date, s.getDate());
        assertEquals(string, s.getString());
    }

    /**
     * Test method for {@link org.deroesch.deployer.entities.Sample#getDate()}.
     */
    @Test
    void testGetDate() {
        Date newDate = new Date();
        sample.setDate(newDate);
        assertEquals(newDate, sample.getDate());
    }

    /**
     * Test method for {@link org.deroesch.deployer.entities.Sample#getString()}.
     */
    @Test
    void testGetString() {
        String newString = new Date().toString();
        sample.setString(newString);
        assertEquals(newString, sample.getString());
    }

    /**
     * Test method for {@link org.deroesch.deployer.entities.Sample#toString()}.
     */
    @Test
    void testToString() {
        assertTrue(sample.toString().startsWith("Sample ["));
    }

    // Lambda template to test an operations on a User
    interface Op {
        void apply(Sample s) throws java.lang.AssertionError;
    }

    /*
     * Catches the throw if a java assert keyword trips. Allows tests to continue.
     * The operator.apply(user) call should always throw.
     */
    void checkAssert(Op operator) {
        final String failMsg = "Allowed null or empty parameter.";

        try {
            operator.apply(sample);
            fail(failMsg);
        } catch (java.lang.AssertionError e) {  // Looking for fails of java assert keyword here.

            // Rethrow of fail(failMsg), which also lands here...
            if (failMsg.equals(e.getMessage()))
                throw e;
        }
    }

}
