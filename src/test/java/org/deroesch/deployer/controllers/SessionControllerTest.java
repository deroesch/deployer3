package org.deroesch.deployer.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.deroesch.deployer.entities.Session;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.deployer.services.ex.ExInvalidState;
import org.deroesch.deployer.services.ex.ExUnauthorizedUser;
import org.deroesch.helpers.Utils;
import org.junit.jupiter.api.Test;

class SessionControllerTest {
	SessionController controller = new SessionController();

	@Test
	void testGetNew() {
		Session session = controller.getNew();
		assertEquals("uCreator", session.getCreatedBy());
		assertFalse(Utils.isEmpty(session.getCreateTime()));
	}

	@Test
	void testGetApproved() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		Session session = controller.getApproved();
		assertEquals("uApprover", session.getApprovedBy());
		assertFalse(Utils.isEmpty(session.getApprovalTime()));
	}

	@Test
	void testGetPending() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		Session session = controller.getPending();
		assertEquals("uExecutor", session.getExecutedBy());
		assertFalse(Utils.isEmpty(session.getStartTime()));
	}

}
