package org.deroesch.helpers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.deroesch.helpers.Utils;
import org.junit.jupiter.api.Test;

class UtilsTest {

    @Test
    void testAssertNotEmptyDate() {
        try {
            Utils.assertNotEmpty((Date) null);
            fail("Allowed empty date.");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore
        }
        try {
            Utils.assertNotEmpty(Utils.DATE_ZERO);
            fail("Allowed empty date.");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore
        }
    }

    @Test
    void testAssertNotEmptyString() {
        try {
            Utils.assertNotEmpty((String) null);
            fail("Allowed empty string.");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore
        }
        try {
            Utils.assertNotEmpty(Utils.EMPTY_STRING);
            fail("Allowed empty string.");
        } catch (java.lang.AssertionError e) {
            // Expected, ignore
        }
    }

    @Test
    void testIsEmptyDate() {
        assertTrue(Utils.isEmpty((Date) null));
        assertTrue(Utils.isEmpty(Utils.DATE_ZERO));
        assertFalse(Utils.isEmpty(new Date()));
    }

    @Test
    void testIsEmptyString() {
        assertTrue(Utils.isEmpty((String) null));
        assertTrue(Utils.isEmpty(Utils.EMPTY_STRING));
        assertFalse(Utils.isEmpty("Hi there"));
    }

}
