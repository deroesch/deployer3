package org.deroesch.toys;

import static org.junit.jupiter.api.Assertions.*;

import org.deroesch.deployer.entities.Fields;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class USAddressTest {

	USAddress addr;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		addr = USAddress.getSample();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetSample() {
		addr = USAddress.getSample();
		assertEquals("500 Grant Street", addr.getLine1());
	}

	@Test
	void testUSAddress() {
		addr = new USAddress();
		assertEquals("", addr.getLine1());
		assertEquals("", addr.getLine2());
		assertEquals("", addr.getCity());
		assertEquals("", addr.getState());
		assertEquals("", addr.getZip());
		assertEquals("", addr.getPlus4());
	}

	@Test
	void testUSAddressWithArgs() {
		addr = new USAddress("1", "2", "3", "4", "55555", "6666");
		assertEquals("1", addr.getLine1());
		assertEquals("2", addr.getLine2());
		assertEquals("3", addr.getCity());
		assertEquals("4", addr.getState());
		assertEquals("55555", addr.getZip());
		assertEquals("6666", addr.getPlus4());
	}

	@Test
	void testGetCity() {
		assertNotEquals("", addr.getCity());
		String arg = Fields.nextString();
		addr.setCity(arg);
		assertEquals(arg, addr.getCity());
	}

	@Test
	void testGetCountryCode() {
		assertEquals("1", addr.getCountryCode());
	}

	@Test
	void testGetLine1() {
		assertNotEquals("", addr.getLine1());
		String arg = Fields.nextString();
		addr.setLine1(arg);
		assertEquals(arg, addr.getLine1());
	
	}

	@Test
	void testGetLine2() {
		assertNotEquals("", addr.getLine2());
		String arg = Fields.nextString();
		addr.setLine2(arg);
		assertEquals(arg, addr.getLine2());
	
	}

	@Test
	void testGetPlus4() {
		assertNotEquals("", addr.getPlus4());
		String arg = "1234";
		addr.setPlus4(arg);
		assertEquals(arg, addr.getPlus4());
	
	}

	@Test
	void testGetState() {
		assertNotEquals("", addr.getState());
		String arg = Fields.nextString();
		addr.setState(arg);
		assertEquals(arg, addr.getState());
	
	}

	@Test
	void testGetZip() {
		assertNotEquals("", addr.getZip());
		String arg = "12345";
		addr.setZip(arg);
		assertEquals(arg, addr.getZip());
	
	} 
	
	@Test
	void testGetCityState() {
		assertEquals("Pittsburgh, PA", addr.getCityState());
	}

	@Test
	void testToString() {
		assertTrue(addr.toString().startsWith("USAddress ["));
	}

}
