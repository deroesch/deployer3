package org.deroesch.toys;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class USPhoneNumberTest {

	/*
	 * USPhoneNumber under test
	 */
	USPhoneNumber phone;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		phone = new USPhoneNumber();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testUSPhoneNumber() {
		assertEquals("1", phone.getCountryCode());
	}

	@Test
	void testUSPhoneNumberWithArgs() {
		PhoneNumber n = new USPhoneNumber("412", "555", "1212");
		assertEquals("+1 (412) 555-1212", n.getDisplayString());
	}

	@Test
	void testGetCountryCode() {
		assertEquals("1", phone.getCountryCode());
	}

	@Test
	void testGetAreaCode() {
		String v = "412";
		phone.setAreaCode(v);
		assertEquals(v, phone.getAreaCode());
	}

	@Test
	void testGetPrefix() {
		String v = "555";
		phone.setPrefix(v);
		assertEquals(v, phone.getPrefix());
	}

	@Test
	void testGetSuffix() {
		String v = "1212";
		phone.setSuffix(v);
		assertEquals(v, phone.getSuffix());
	}

	@Test
	void testGetFullString() {
		PhoneNumber n = new USPhoneNumber("814", "555", "2121");
		assertEquals("+1 (814) 555-2121", n.getDisplayString());
	}

}
