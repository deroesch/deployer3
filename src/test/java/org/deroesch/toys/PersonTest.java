package org.deroesch.toys;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.deroesch.helpers.Utils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonTest {

	/*
	 * Person under test
	 */
	Person person;
	PhoneNumber pittPhone = new USPhoneNumber("412", "555", "1212");
	PhoneNumber eriePhone = new USPhoneNumber("814", "555", "1212");
	Address bnyPitt = USAddress.getSample();

	/*
	 * Some constants
	 */
	private final String id = "42";
	private final String firstName = "Jack";
	private final String lastName = "Frost";
	private final String socSecNo = "123456789";
	private final String cityOfBirth = "New York";
	private final Date dateOfBirth = new Date();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		person = new Person(id, firstName, lastName, socSecNo, cityOfBirth, dateOfBirth);
		person.addPhoneNumber("Pittsburgh", pittPhone);
		person.addPostalAddress("Pittsburgh", bnyPitt);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testPerson() {
		Person p = new Person();
		assertEquals(Utils.EMPTY_STRING, p.getFirstName());
		assertEquals(Utils.EMPTY_STRING, p.getLastName());
		assertEquals(Utils.EMPTY_STRING, p.getSocSecNo());
		assertEquals(Utils.EMPTY_STRING, p.getCityOfBirth());
		assertEquals(Utils.DATE_ZERO, p.getDateOfBirth());
	}

	@Test
	void testPersonWithArgs() {
		Person p = new Person(id, firstName, lastName, socSecNo, cityOfBirth, dateOfBirth);
		assertEquals(this.firstName, p.getFirstName());
		assertEquals(this.lastName, p.getLastName());
		assertEquals(this.socSecNo, p.getSocSecNo());
		assertEquals(this.cityOfBirth, p.getCityOfBirth());
		assertEquals(this.dateOfBirth, p.getDateOfBirth());
	}

	@Test
	void testGetId() {
		assertEquals(this.id, person.getId());
	}

	@Test
	void testGetFirstName() {
		assertEquals(this.firstName, person.getFirstName());
	}

	@Test
	void testGetLastName() {
		assertEquals(this.lastName, person.getLastName());
	}

	@Test
	void testGetSocSecNo() {
		assertEquals(this.socSecNo, person.getSocSecNo());
	}

	@Test
	void testGetCityOfBirth() {
		assertEquals(this.cityOfBirth, person.getCityOfBirth());
	}

	@Test
	void testGetDateOfBirth() {
		assertEquals(this.dateOfBirth, person.getDateOfBirth());
	}

	@Test
	void testGetUuid() {
		assertTrue(person.getUuid().toString().length() > 0);
	}

	@Test
	void testGetFullName() {
		String full = String.format("%s %s", person.getFirstName(), person.getLastName());
		assertEquals(full, person.getDisplayName());
	}

	@Test
	void testGetEmailAddresses() {
		assertEquals(0, person.getEmailAddresses().size());
	}

	@Test
	void testAddEmailAddress() {
		fail("Not yet implemented");
	}

	@Test
	void testGetEmailAddress() {
		fail("Not yet implemented");
	}

	@Test
	void testRemoveEmailAddress() {
		fail("Not yet implemented");
	}

	@Test
	void testGetPostalAddresses() {
		assertEquals(1, person.getPostalAddresses().size());
	}

	@Test
	void testAddPostalAddress() {
		fail("Not yet implemented");
	}

	@Test
	void testGetPostalAddress() {
		assertEquals(bnyPitt, person.getPostalAddress("Pittsburgh"));
		assertNull(person.getPostalAddress("Nothing"));
	}

	@Test
	void testRemovePostalAddress() {
		fail("Not yet implemented");
	}

	@Test
	void testGetPhoneNumbers() {
		assertEquals(1, person.getPhoneNumbers().size());
	}

	@Test
	void testAddPhoneNumber() {
		assertFalse(person.getPhoneNumbers().containsKey("Erie"));
		person.addPhoneNumber("Erie", this.eriePhone);
		assertTrue(person.getPhoneNumbers().containsKey("Erie"));
		assertEquals(2, person.getPhoneNumbers().size());
	}

	@Test
	void testGetPhoneNumber() {
		PhoneNumber p = person.getPhoneNumber("Pittsburgh");
		assertEquals(this.pittPhone, p);
		assertNull(person.getPhoneNumber("Nothing"));
	}

	@Test
	void testRemovePhoneNumber() {
		PhoneNumber p = person.removePhoneNumber("Pittsburgh");
		assertEquals(this.pittPhone, p);
		assertEquals(0, person.getPhoneNumbers().size());

		assertNull(person.removePhoneNumber("badKey"));
	}

	@Test
	void testToString() {
		assertTrue(person.toString().startsWith("Person ["));
	}

}
