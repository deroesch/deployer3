package org.deroesch.deployer.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.deroesch.deployer.entities.Request;
import org.deroesch.deployer.entities.Role;
import org.deroesch.deployer.entities.Session;
import org.deroesch.deployer.entities.User;
import org.deroesch.deployer.services.SessionService;
import org.deroesch.deployer.services.SessionServiceImpl;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.deployer.services.ex.ExInvalidState;
import org.deroesch.deployer.services.ex.ExUnauthorizedUser;

/**
 * Root resource (exposed at "sessions" path).
 * 
 * Not much error handling here.
 */
@Path("sessions")
public class SessionController {

	private final User approver = new User("uApprover");
	private final User creator = new User("uCreator");
	private final User executor = new User("uExecutor");
	// TODO Get rid of this
	private Request request = new Request();

	private final SessionService service = new SessionServiceImpl();

	// Extra initialization actions.
	{
		creator.addRole(Role.SessionCreator);
		approver.addRole(Role.Approver);
		executor.addRole(Role.Executor);
	}

	@GET
	@Path("approved")
	@Produces(MediaType.APPLICATION_JSON)
	public Session getApproved() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		return newApprovedSession();
	}

	@GET
	@Path("new")
	@Produces(MediaType.APPLICATION_JSON)
	public Session getNew() {
		return new Session(request, creator.getUname());
	}

	@GET
	@Path("pending")
	@Produces(MediaType.APPLICATION_JSON)
	public Session getPending() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		return newPendingSession();
	}

	/*
	 * Helper for getting a good approved session. Starts with a new session and
	 * uses service operations to move it to an approved state.
	 */
	Session newApprovedSession() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		Session session = service.create(creator, null);
		service.edit(session, creator);
		service.submit(session, creator);
		service.approve(session, approver);
		return session;
	}

	/*
	 * Helper for getting a good pending session. Starts with an approved session
	 * and executes it.
	 */
	Session newPendingSession() throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {
		Session session = newApprovedSession();
		service.execute(session, executor);
		return session;
	}
}
