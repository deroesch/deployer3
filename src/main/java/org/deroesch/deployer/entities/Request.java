package org.deroesch.deployer.entities;

import java.util.ArrayList;
import java.util.List;

import org.deroesch.deployer.dcases.DCase;
import org.deroesch.helpers.Utils;

public class Request {
	private String apiToken;
	private String artifactID;
	private DCase dCase;
	private String key;
	private String nodeID;
	private String pipeline;
	private List<Session> sessions = new ArrayList<Session>();
	private String webhookToken;

	public Request() {
		super();
	}

	public Request(String artifactID, String nodeID, String pipeline, String apiToken, String webhookToken,
			DCase dCase) {
		super();
		Utils.assertNotEmpty(artifactID); 
		Utils.assertNotEmpty(nodeID); 
		Utils.assertNotEmpty(pipeline); 
		Utils.assertNotEmpty(apiToken); 
		Utils.assertNotEmpty(webhookToken); 
		Utils.assertNotEmpty(dCase); 
		this.artifactID = artifactID;
		this.nodeID = nodeID;
		this.pipeline = pipeline;
		this.apiToken = apiToken;
		this.webhookToken = webhookToken;
		this.dCase = dCase;
	}

	public void addSession(Session session) {
		assert null != session;
		assert !this.getSessions().contains(session);
		assert session.getRequest() == this;

		sessions.add(session);
	}

	public String getApiToken() {
		Utils.assertNotEmpty(apiToken);
		return apiToken;
	}

	public String getArtifactID() {
		Utils.assertNotEmpty(artifactID);
		return artifactID;
	}

	public DCase getCase() {
		Utils.assertNotEmpty(dCase);
		return dCase;
	}

	public String getKey() {
		Utils.assertNotEmpty(key);
		return key;
	}

	public Session getLastSession() {
		int size = getSessionCount();
		return size > 0 ? getSessions().get(size - 1) : null;
	}

	public String getNodeID() {
		Utils.assertNotEmpty(nodeID);
		return nodeID;
	}

	public String getPipeline() {
		Utils.assertNotEmpty(pipeline);
		return pipeline;
	}

	public int getSessionCount() {
		return sessions.size();
	}

	public List<Session> getSessions() {
		return sessions;
	}

	/**
	 * My state is the state of my most recent deployment session. If I don't have a
	 * session, return CREATED.
	 * 
	 * @return
	 */
	public SessionState getState() {
		Session s = getLastSession();
		return (null != s) ? s.getState() : SessionState.CREATED;
	}

	public String getWebhookToken() {
		Utils.assertNotEmpty(webhookToken);
		return webhookToken;
	}

	public void setApiToken(String apiToken) {
		Utils.assertNotEmpty(apiToken); 
		this.apiToken = apiToken;
	}

	public void setArtifactID(String artifactID) {
		Utils.assertNotEmpty(artifactID); 
		this.artifactID = artifactID;
	}

	public void setCase(DCase dCase) {
		Utils.assertNotEmpty(dCase); 
		this.dCase = dCase;
	}

	public void setKey(String key) {
		Utils.assertNotEmpty(key); 
		this.key = key;
	}

	public void setNodeID(String nodeID) {
		Utils.assertNotEmpty(nodeID); 
		this.nodeID = nodeID;
	}

	public void setPipeline(String pipeline) {
		Utils.assertNotEmpty(pipeline); 
		this.pipeline = pipeline;
	}

	public void setWebhookToken(String webhookToken) {
		Utils.assertNotEmpty(webhookToken); 
		this.webhookToken = webhookToken;
	}

	@Override
	public String toString() {
		return "DRequest [artifactID=" + artifactID + ", nodeID=" + nodeID + ", pipeline=" + pipeline + ", case="
				+ dCase + "]";
	}

	/*
	 * Helpers
	 */

}
