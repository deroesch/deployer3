package org.deroesch.deployer.entities;

/**
 * Stages in a session's life-cycle.
 */
public enum SessionStage {
    FINISHED, IDLE, STARTED
}
