package org.deroesch.deployer.entities;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.deroesch.deployer.dcases.DCase;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.helpers.Utils;

@XmlRootElement
public class Session {

	private Date approvalTime = Utils.DATE_ZERO;
	private String approvedBy = Utils.EMPTY_STRING;
	private String canceledBy = Utils.EMPTY_STRING;

	private String code = Utils.EMPTY_STRING;
	/*
	 * All others start as "empty"
	 */
	private String createdBy = Utils.EMPTY_STRING;
	private Date createTime = new Date();
	private String editedBy = Utils.EMPTY_STRING;
	private Date editTime = Utils.DATE_ZERO;
	private String executedBy = Utils.EMPTY_STRING;
	private Date finishTime = Utils.DATE_ZERO;
	private String message = Utils.EMPTY_STRING;
	/*
	 * My Deployment Request
	 */
	private Request request;
	/*
	 * Fields initialized on creation
	 */
	private SessionStage sessionStage = SessionStage.IDLE;
	private Date startTime = Utils.DATE_ZERO;
	private SessionState state = SessionState.CREATED;

	private String submittedBy = Utils.EMPTY_STRING;

	/**
	 * Null constructor is hidden
	 * 
	 * @throws ExBadArgs
	 */
	Session(Request request) {
		this(request, User.DEFAULT_UNAME);
	}

	/**
	 * Public constructor only accepts ID of creating User
	 * 
	 * @param createdBy
	 * @throws ExBadArgs
	 */
	public Session(Request request, String createdBy) {
		super();
		Utils.assertNotEmpty(createdBy);
		this.createdBy = createdBy;
		this.request = request;
		
		request.addSession(this);
	}

	/*
	 * Accessors. Some are non-public.
	 */

	public String getApiToken() {
		return request.getApiToken();
	}

	public Date getApprovalTime() {
		return approvalTime;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	/*
	 * Getters for my contained Deployment Request
	 */
	public String getArtifactID() {
		return request.getArtifactID();
	}

	public String getCanceledBy() {
		return canceledBy;
	}

	public DCase getCase() {
		return request.getCase();
	}

	public String getCode() {
		return code;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public Date getEditTime() {
		return editTime;
	}

	public String getExecutedBy() {
		return executedBy;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public String getMessage() {
		return message;
	}

	public String getNodeID() {
		return request.getNodeID();
	}

	public String getPipeline() {
		return request.getPipeline();
	}

	public Request getRequest() {
		return this.request;
	}

	public SessionStage getStage() {
		return sessionStage;
	}

	public Date getStartTime() {
		return startTime;
	}

	public SessionState getState() {
		return state;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public String getWebhookToken() {
		return request.getWebhookToken();
	}

	public void setApprovalTime(Date approvalTime) {
		Utils.assertNotEmpty(approvalTime);
		this.approvalTime = approvalTime;
	}

	public void setApprovedBy(String approvedBy) {
		Utils.assertNotEmpty(approvedBy);
		this.approvedBy = approvedBy;
	}

	public void setCanceledBy(String canceledBy) {
		Utils.assertNotEmpty(canceledBy);
		this.canceledBy = canceledBy;
	}

	public void setCode(String code) {
		Utils.assertNotEmpty(code);
		this.code = code;
	}

	void setCreatedBy(String createdBy) {
		Utils.assertNotEmpty(createdBy);
		this.createdBy = createdBy;
	}

	void setCreateTime(Date createTime) {
		Utils.assertNotEmpty(createTime);
		this.createTime = createTime;
	}

	public void setEditedBy(String editedBy) {
		Utils.assertNotEmpty(editedBy);
		this.editedBy = editedBy;
	}

	public void setEditTime(Date editTime) {
		Utils.assertNotEmpty(editTime);
		this.editTime = editTime;
	}

	public void setExecutedBy(String executedBy) {
		Utils.assertNotEmpty(executedBy);
		this.executedBy = executedBy;
	}

	public void setFinishTime(Date finishTime) {
		Utils.assertNotEmpty(finishTime);
		this.finishTime = finishTime;
	}

	public void setMessage(String message) {
		Utils.assertNotEmpty(message);
		this.message = message;
	}

	public void setStage(SessionStage sessionStage) {
		assert null != sessionStage;
		this.sessionStage = sessionStage;
	}

	public void setStartTime(Date startTime) {
		Utils.assertNotEmpty(startTime);
		this.startTime = startTime;
	}

	public void setState(SessionState state) {
		assert null != state;
		this.state = state;
	}

	public void setSubmittedBy(String submittedBy) {
		Utils.assertNotEmpty(submittedBy);
		this.submittedBy = submittedBy;
	}

	/*
	 * Helpers
	 */

	@Override
	public String toString() {
		String request = this.request.toString();
		String session = "DSession [stage=" + sessionStage + ", state=" + state + ", createTime=" + createTime
				+ ", createdBy=" + createdBy + ", editedBy=" + editedBy + ", executedBy=" + executedBy + ", approvedBy="
				+ approvedBy + ", canceledBy=" + canceledBy + ", code=" + code + ", message=" + message + ", editTime="
				+ editTime + ", startTime=" + startTime + ", finishTime=" + finishTime + ", approvalTime="
				+ approvalTime + "]";

		return "[" + request + " " + session + "]";
	}

}
