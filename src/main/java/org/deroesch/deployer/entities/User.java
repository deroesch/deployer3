package org.deroesch.deployer.entities;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.deroesch.helpers.Utils;

/**
 * Little of interest here. Just a user name and some roles
 */
@XmlRootElement
public class User {

    public static final String DEFAULT_UNAME = "anonymous";

    private Set<Role> roles = new HashSet<>();
    private String    uName;

    /*
     * Constructors
     */
    public User() {
        this(DEFAULT_UNAME);
        assert hasRole(Role.User);
    }

    public User(String uName) {
        super();
        Utils.assertNotEmpty(uName);

        this.uName = uName;
        this.addRole(Role.User);
        assert hasRole(Role.User);
    }

    public void addRole(Role r) {
        assert r != null;
        roles.add(r);
    }

    public Set<Role> getRoles() {
        assert roles != null;
        return roles;
    }

    /*
     * Accessors. Some setters are non-public.
     */
    public String getUname() {
        Utils.assertNotEmpty(uName);
        return uName;
    }

    /*
     * Role maintenance
     */
    public boolean hasRole(Role r) {
        assert r != null;
        return roles.contains(r);
    }

    public void removeRole(Role r) {
        assert r != null;
        roles.remove(r);
    }

    void setRoles(Set<Role> roles) {
        assert roles != null;
        this.roles = roles;
    }

    void setUname(String uName) {
        Utils.assertNotEmpty(uName);

        this.uName = uName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "User [uName=" + this.uName + ", roles=" + this.roles + "]";
    }

}
