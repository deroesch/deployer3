package org.deroesch.deployer.entities;

import java.util.Date;

import org.deroesch.helpers.Utils;

/**
 * Canonical sample
 */
public class Sample {
    private Date   date;
    private String string;

    // Make non-public
    Sample() {
        super();
    }

    /**
     * Public constructor
     * 
     * @param date   Must be non-empty
     * @param string Must be non-empty
     */
    public Sample(Date date, String string) {
        super();
        Utils.assertNotEmpty(date);    
        Utils.assertNotEmpty(string);  
        this.date = date;
        this.string = string;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        Utils.assertNotEmpty(date);   
        return this.date;
    }

    /**
     * @return the string
     */
    public String getString() {
        Utils.assertNotEmpty(string);   
        return this.string;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        Utils.assertNotEmpty(date);    
        this.date = date;
    }

    /**
     * @param string the string to set
     */
    public void setString(String string) {
        Utils.assertNotEmpty(string);    
        this.string = string;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Sample [date=" + this.date + ", string=" + this.string + "]";
    }

}
