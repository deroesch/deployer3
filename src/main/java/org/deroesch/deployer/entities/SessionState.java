package org.deroesch.deployer.entities;

/**
 * Mutually-exclusive states a session may hold.
 */
public enum SessionState {
	APPROVED, CANCELED, CREATED, DELETED, FAILED, PENDING, RUNNING, SUBMITTED, SUCCESS
}
