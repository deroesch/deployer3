package org.deroesch.deployer.entities;

/**
 * Roles played by Users.
 */
public enum Role {
    AppCreator, Approver, Executor, SessionCreator, User
}
