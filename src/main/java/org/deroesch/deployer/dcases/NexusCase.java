package org.deroesch.deployer.dcases;

public class NexusCase implements IDCase {
	private String artifactID;
	private String classifier;
	private String groupID;
	private String key;
	private String packaging;
	private String version;

	public NexusCase() {
		super();
	}

	public NexusCase(String groupID, String artifactID, String version, String classifier, String packaging) {
		super();
		this.groupID = groupID;
		this.artifactID = artifactID;
		this.version = version;
		this.classifier = classifier;
		this.packaging = packaging;
	}

	public String getArtifactID() {
		return artifactID;
	}

	@Override
	public DCase getCase() {
		return DCase.Nexus;
	}

	public String getClassifier() {
		return classifier;
	}

	public String getGroupID() {
		return groupID;
	}

	public String getKey() {
		return key;
	}

	public String getPackaging() {
		return packaging;
	}

	public String getVersion() {
		return version;
	}

	public void setArtifactID(String artifactID) {
		this.artifactID = artifactID;
	}

	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
