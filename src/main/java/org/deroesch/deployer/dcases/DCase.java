package org.deroesch.deployer.dcases;

import org.deroesch.deployer.services.ex.ExBadArgs;

/**
 * Deployment Case (DCase)
 */
public enum DCase {
	AppEngine, Nexus, None, VM, Wizard;

	/**
	 * 
	 * @param dc
	 * @return
	 * @throws ExBadArgs
	 */
	public static IDCase create(DCase dc) throws ExBadArgs {
		
		assert null != dc;
		
		switch (dc) {
		case AppEngine:
			return new AppEngineCase();
		case VM:
			return new VmCase();
		case Wizard:
			return new WizardCase();
		case Nexus:
			return new NexusCase();
		default:
			throw new ExBadArgs();
		}
	}

}
