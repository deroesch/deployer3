package org.deroesch.deployer.dcases;

import org.deroesch.helpers.Utils;

public class AppEngineCase implements IDCase {
	private String app;
	private String family;
	private String key;
	private String name;
	private String orgID;

	public AppEngineCase() {
		super();
	}

	public AppEngineCase(String name, String orgID, String family, String app) {
		super();
		Utils.assertNotEmpty(name); 
		Utils.assertNotEmpty(orgID); 
		Utils.assertNotEmpty(family); 
		Utils.assertNotEmpty(app); 
		this.name = name;
		this.orgID = orgID;
		this.family = family;
		this.app = app;
	}

	public String getApp() {
		Utils.assertNotEmpty(app); 
		return app;
	}

	@Override
	public DCase getCase() {
		return DCase.AppEngine;
	}

	public String getFamily() {
		Utils.assertNotEmpty(family);
		return family;
	}

	public String getKey() {
		Utils.assertNotEmpty(key);
		return key;
	}

	public String getName() {
		Utils.assertNotEmpty(name);
		return name;
	}

	public String getOrgID() {
		Utils.assertNotEmpty(orgID);
		return orgID;
	}

	public void setApp(String app) {
		Utils.assertNotEmpty(app); 
		this.app = app;
	}

	public void setFamily(String family) {
		Utils.assertNotEmpty(family); 
		this.family = family;
	}

	public void setKey(String key) {
		Utils.assertNotEmpty(key); 
		this.key = key;
	}

	public void setName(String name) {
		Utils.assertNotEmpty(name); 
		this.name = name;
	}

	public void setOrgID(String orgID) {
		Utils.assertNotEmpty(orgID); 
		this.orgID = orgID;
	}

	@Override
	public String toString() {
		return "AppEngineCase [name=" + name + ", orgID=" + orgID + ", family=" + family + ", app=" + app + "]";
	}

}
