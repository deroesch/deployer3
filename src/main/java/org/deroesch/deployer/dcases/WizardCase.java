package org.deroesch.deployer.dcases;

public class WizardCase implements IDCase {

	@Override
	public DCase getCase() {
		return DCase.Wizard;
	}

}
