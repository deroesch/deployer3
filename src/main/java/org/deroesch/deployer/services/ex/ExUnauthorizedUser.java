package org.deroesch.deployer.services.ex;
/**
 * Thrown when a user tries to do something they're not allowed to do.
 */
public class ExUnauthorizedUser extends Exception {

	/**
	 * Generated ID
	 */
	private static final long serialVersionUID = 1120082426423351519L;

}
