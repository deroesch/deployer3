package org.deroesch.deployer.services.ex;

/**
 * Thrown when invalid args are used in an operation.
 */
public class ExBadArgs extends Exception {

	/**
	 * Generated ID
	 */
	private static final long serialVersionUID = 8998448953421538570L;

}
