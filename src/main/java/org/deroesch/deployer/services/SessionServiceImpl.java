package org.deroesch.deployer.services;

import java.util.Date;
import java.util.Map;

import org.deroesch.deployer.entities.Request;
import org.deroesch.deployer.entities.Role;
import org.deroesch.deployer.entities.Session;
import org.deroesch.deployer.entities.SessionStage;
import org.deroesch.deployer.entities.SessionState;
import org.deroesch.deployer.entities.User;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.deployer.services.ex.ExInvalidState;
import org.deroesch.deployer.services.ex.ExUnauthorizedUser;

import kong.unirest.Unirest;

public class SessionServiceImpl implements SessionService {

	// TODO Get rid of this
	private Request request = new Request();

	public SessionServiceImpl() {
		// Nothing of interest here yet
	}

	@Override
	public void approve(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.Approver);
		requiresSessionState(r, SessionState.SUBMITTED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "approved");
		r.setApprovedBy(u.getUname());
		r.setApprovalTime(d);
		r.setState(SessionState.APPROVED);
	}

	@Override
	public void cancel(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.Approver);
		requiresSessionStage(r, SessionStage.IDLE);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "canceled");
		r.setCanceledBy(u.getUname());
		r.setFinishTime(d);
		r.setStage(SessionStage.FINISHED);
		r.setState(SessionState.CANCELED);
	}

	@Override
	public Session create(User u, Map<String, String> args) throws ExUnauthorizedUser, ExBadArgs, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);

		// Return the new session
		return new Session(request, u.getUname());
	}

	@Override
	public void delete(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);
		requiresSessionState(r, SessionState.CREATED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "deleted");
		r.setState(SessionState.DELETED);

		// Delete record
	}

	@Override
	public void edit(Session r, User u) throws ExUnauthorizedUser, ExInvalidState, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);
		requiresSessionState(r, SessionState.CREATED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "edited");
		r.setEditedBy(u.getUname());
		r.setEditTime(d);
		r.setState(SessionState.CREATED);

		// Open for editing
		// Does not change session state
	}

	@Override
	public void execute(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.Executor);
		requiresSessionState(r, SessionState.APPROVED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "executed");
		r.setExecutedBy(u.getUname());
		r.setStartTime(d);
		r.setStage(SessionStage.STARTED);
		r.setState(SessionState.PENDING);

		// Make HTTP call to GitLab to trigger session
		//
		// Something like this, for the correct URL and token
		// See http://kong.github.io/unirest-java/
		Unirest.post("http://httpbin.org/post").header("accept", "application/json").queryString("apiKey", "123")
				.field("parameter", "value").field("firstname", "Gary").asJson();

	}

	void log(Session r, User u, Date d, String action) {
		// TODO Auto-generated method stub
	}

	@Override
	public void recall(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);
		requiresSessionState(r, SessionState.SUBMITTED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "recalled");
		r.setEditedBy(u.getUname());
		r.setEditTime(d);

		// A recall sets the state back to CREATED
		r.setState(SessionState.CREATED);
	}

	@Override
	public void reject(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.Approver);
		requiresSessionState(r, SessionState.SUBMITTED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "rejected");
		r.setEditedBy(u.getUname());
		r.setEditTime(d);

		// A rejection sets the state back to CREATED
		r.setState(SessionState.CREATED);
	}

	/**
	 * Throw is session is not in the required stage.
	 * 
	 * @param r
	 * @param s
	 * @throws ExInvalidState
	 */
	void requiresSessionStage(Session r, SessionStage s) throws ExInvalidState {
		if (r.getStage() != s)
			throw new ExInvalidState();

	}

	/**
	 * Throw is session is not in the required state.
	 * 
	 * @param r
	 * @param s
	 * @throws ExInvalidState
	 */
	void requiresSessionState(Session r, SessionState s) throws ExInvalidState {
		if (r.getState() != s)
			throw new ExInvalidState();

	}

	/**
	 * Throw is user lacks the required role.
	 * 
	 * @param u
	 * @param r
	 * @throws ExUnauthorizedUser
	 */
	void requiresUserRole(User u, Role r) throws ExUnauthorizedUser {
		if (!u.hasRole(r))
			throw new ExUnauthorizedUser();

	}

	@Override
	public void saveAndValidate(Session r, User u) throws ExUnauthorizedUser, ExInvalidState, ExBadArgs {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);
		requiresSessionState(r, SessionState.CREATED);
		validateSessionFields(r);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "saved");
		r.setEditedBy(u.getUname());
		r.setEditTime(d);

		// Save to database
		// Does not change session state
	}

	@Override
	public void submit(Session r, User u) throws ExUnauthorizedUser, ExInvalidState {

		// Check the preconditions
		requiresUserRole(u, Role.SessionCreator);
		requiresSessionState(r, SessionState.CREATED);

		// Update the session fields
		Date d = new Date();
		log(r, u, d, "submitted");
		r.setSubmittedBy(u.getUname());
		r.setEditTime(d);
		r.setState(SessionState.SUBMITTED);
	}

	void validateSessionFields(Session r) {
		// TODO Auto-generated method stub
	}

}
