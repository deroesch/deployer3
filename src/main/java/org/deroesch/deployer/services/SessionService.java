package org.deroesch.deployer.services;

import java.util.Map;

import org.deroesch.deployer.entities.Session;
import org.deroesch.deployer.entities.User;
import org.deroesch.deployer.services.ex.ExBadArgs;
import org.deroesch.deployer.services.ex.ExInvalidState;
import org.deroesch.deployer.services.ex.ExUnauthorizedUser;

/**
 * A simple service for manipulating sessions.
 */
public interface SessionService {

	/**
	 * Approves a deployment session for execution.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void approve(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Cancels a deployment session.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void cancel(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Creates a new deployment session record.
	 * 
	 * @param u    The user making the request
	 * @param args Arguments used for creating the record
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExBadArgs            If the incoming arguments are unusable
	 * @return A new deployment session record.
	 * @throws ExInvalidState 
	 */
	Session create(User u, Map<String, String> args) throws ExUnauthorizedUser, ExBadArgs, ExInvalidState;

	/**
	 * Deletes a deployment session record.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void delete(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Opens a deployment session record for editing.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void edit(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Executes a deployment session.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void execute(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Recalls a deployment session record back to the submitter.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void recall(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Rejects a deployment session record back to the submitter.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void reject(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

	/**
	 * Saves an edited deployment session record.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExBadArgs            If the field values are unusable
	 * @throws ExInvalidState 
	 */
	void saveAndValidate(Session r, User u) throws ExUnauthorizedUser, ExBadArgs, ExInvalidState;

	/**
	 * Submits a deployment session record for execution.
	 * 
	 * @param r The deployment session record
	 * @param u The user making the request
	 * @throws ExUnauthorizedUser If user is not allowed to do this
	 * @throws ExInvalidState 
	 */
	void submit(Session r, User u) throws ExUnauthorizedUser, ExInvalidState;

}
