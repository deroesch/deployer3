package org.deroesch.helpers;

import java.util.Date;

import org.deroesch.deployer.dcases.DCase;

/**
 * Simple system-wide utilities.
 */
public class Utils {

	/**
	 * System definition of an empty date.
	 */
	public static final Date DATE_ZERO = new Date(0L);

	/**
	 * System definition of an empty string.
	 */
	public static final String EMPTY_STRING = "";

	/**
	 * System definition of a non-empty string.
	 */
	public static final String NON_EMPTY_STRING = "Hello world!";

	/**
	 * Enforce non-empty Dates (for contracts)
	 * 
	 * @param date The date to test
	 */
	public static final void assertNotEmpty(Date date) {
		assert !isEmpty(date);
	}

	/**
	 * Predicate to test for empty Strings
	 * 
	 * @param str The string to test
	 * @return true or false
	 */
	public static final void assertNotEmpty(DCase f) {
		assert null != f;
	}

	/**
	 * Enforce non-empty Strings
	 * 
	 * @param str The string to test
	 */
	public static final void assertNotEmpty(String str) {
		assert !isEmpty(str);
	}

	/**
	 * Predicate to test for empty dates.
	 * 
	 * @param str The date to test
	 * @return true or false
	 */
	public static final boolean isEmpty(Date date) {
		return null == date || date.equals(Utils.DATE_ZERO);
	}

	/**
	 * Predicate to test for empty Strings
	 * 
	 * @param str The string to test
	 * @return true or false
	 */
	public static final boolean isEmpty(String str) {
		return null == str || str.trim().equals(Utils.EMPTY_STRING);
	}

}
