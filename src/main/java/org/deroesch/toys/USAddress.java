package org.deroesch.toys;

import org.deroesch.helpers.Utils;

public class USAddress implements Address {

	/*
	 * Static helper
	 */
	public static USAddress getSample() {
		USAddress sample = new USAddress();
		sample.setLine1("500 Grant Street");
		sample.setLine2("MS 151-08");
		sample.setCity("Pittsburgh");
		sample.setState("PA");
		sample.setZip("15258");
		sample.setPlus4("0000");
		return sample;
	}

	/*
	 * Fields
	 */
	private String city = Utils.EMPTY_STRING;
	private String line1 = Utils.EMPTY_STRING;
	private String line2 = Utils.EMPTY_STRING;
	private String plus4 = Utils.EMPTY_STRING;
	private String state = Utils.EMPTY_STRING;
	private String zip = Utils.EMPTY_STRING;

	/*
	 * Constructors
	 */
	public USAddress() {
		super();
	}

	public USAddress(String line1, String line2, String city, String state, String zip, String plus4) {
		super();
		this.setLine1(line1);
		this.setLine2(line2);
		this.setCity(city);
		this.setState(state);
		this.setZip(zip);
		this.setPlus4(plus4);
	}

	/*
	 * Gets
	 */
	public String getCity() {
		return city;
	}

	@Override
	public String getCountryCode() {
		return "1";
	}

	public String getLine1() {
		return line1;
	}

	public String getLine2() {
		return line2;
	}

	public String getPlus4() {
		return plus4;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public String getCityState() {
		return String.format("%s, %s", this.getCity(), this.getState());
	}

	/*
	 * Sets
	 */
	public void setCity(String city) {
		assert null != city;
		this.city = city;
	}

	public void setLine1(String line1) {
		assert null != line1;
		this.line1 = line1;
	}

	public void setLine2(String line2) {
		assert null != line2;
		this.line2 = line2;
	}

	public void setPlus4(String plus4) {
		assert null != plus4;
		assert 4 == plus4.length();
		this.plus4 = plus4;
	}

	public void setState(String state) {
		assert null != state;
		this.state = state;
	}

	public void setZip(String zip) {
		assert null != zip;
		assert 5 == zip.length();
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "USAddress [line1=" + line1 + ", line2=" + line2 + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", plus4=" + plus4 + "]";
	}

}
