package org.deroesch.toys;

public interface Address {

	/*
	 * Reveals address format. Uses telephone country codes.
	 */
	String getCountryCode();
}
