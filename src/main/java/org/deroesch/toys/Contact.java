package org.deroesch.toys;

import java.util.HashMap;
import java.util.Map;

/*
 * Something that has an email address, a street address or a phone number.
 */
public abstract class Contact extends Entity {

	protected Map<String, String> emailAddresses = new HashMap<>();
	protected Map<String, PhoneNumber> phoneNumbers = new HashMap<>();
	protected Map<String, Address> postalAddresses = new HashMap<>();

	/*
	 * Adds
	 */
	public void addEmailAddress(String key, String emailAddress) {
		assert null != key;
		assert null != emailAddress;
		this.getEmailAddresses().put(key, emailAddress);
	}

	public void addPhoneNumber(String key, PhoneNumber address) {
		assert null != key;
		assert null != address;
		this.getPhoneNumbers().put(key, address);
	}

	public void addPostalAddress(String key, Address address) {
		assert null != key;
		assert null != address;
		this.getPostalAddresses().put(key, address);
	}

	/*
	 * What I'm called in documents. My name.
	 */
	public abstract String getDisplayName();

	/*
	 * Gets
	 */
	public String getEmailAddress(String key) {
		assert null != key;
		return this.getEmailAddresses().get(key);
	}

	public Map<String, String> getEmailAddresses() {
		return emailAddresses;
	}

	public PhoneNumber getPhoneNumber(String key) {
		assert null != key;
		return this.getPhoneNumbers().get(key);
	}

	public Map<String, PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public Address getPostalAddress(String key) {
		assert null != key;
		return this.getPostalAddresses().get(key);
	}

	public Map<String, Address> getPostalAddresses() {
		return postalAddresses;
	}

	/*
	 * Removes
	 */
	public String removeEmailAddress(String key) {
		assert null != key;
		return this.getEmailAddresses().remove(key);
	}

	public PhoneNumber removePhoneNumber(String key) {
		assert null != key;
		return this.getPhoneNumbers().remove(key);
	}

	public Address removePostalAddress(String key) {
		assert null != key;
		return this.getPostalAddresses().remove(key);
	}

}
