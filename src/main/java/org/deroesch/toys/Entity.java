package org.deroesch.toys;

import java.util.UUID;

import org.deroesch.helpers.Utils;

/*
 * Something we expect to store in a database.
 */
public class Entity {

	protected String id = Utils.EMPTY_STRING;
	private UUID uuid = UUID.randomUUID();

	public String getId() {
		return id;
	}

	public UUID getUuid() {
		return uuid;
	}

}
