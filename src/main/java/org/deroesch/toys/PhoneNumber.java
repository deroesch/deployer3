package org.deroesch.toys;

public interface PhoneNumber {

	/*
	 * Telephone country codes, i.e.:
	 * https://en.wikipedia.org/wiki/List_of_country_calling_codes
	 */
	String getCountryCode();

	/*
	 * How I appear in documents
	 */
	String getDisplayString();

}
