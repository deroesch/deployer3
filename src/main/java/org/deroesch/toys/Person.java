package org.deroesch.toys;

import java.util.Date;

import org.deroesch.helpers.Utils;

public class Person extends Contact {

	private String cityOfBirth = Utils.EMPTY_STRING;
	private Date dateOfBirth = Utils.DATE_ZERO;
	private String firstName = Utils.EMPTY_STRING;
	private String lastName = Utils.EMPTY_STRING;
	private String socSecNo = Utils.EMPTY_STRING;

	/*
	 * Constructors
	 */
	Person() {
		super();
	}

	public Person(String id, String firstName, String lastName, String socSecNo, String cityOfBirth, Date dateOfBirth) {
		super();
		this.id = id;
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setSocSecNo(socSecNo);
		this.setCityOfBirth(cityOfBirth);
		this.setDateOfBirth(dateOfBirth);
	}

	/*
	 * Gets
	 */
	public String getCityOfBirth() {
		return cityOfBirth;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	@Override
	public String getDisplayName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getSocSecNo() {
		return socSecNo;
	}

	/*
	 * Sets
	 */
	public void setCityOfBirth(String cityOfBirth) {
		assert null != cityOfBirth;
		this.cityOfBirth = cityOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		assert null != dateOfBirth;
		this.dateOfBirth = dateOfBirth;
	}

	public void setFirstName(String firstName) {
		assert null != firstName;
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		assert null != lastName;
		this.lastName = lastName;
	}

	public void setSocSecNo(String socSecNo) {
		assert null != socSecNo;
		this.socSecNo = socSecNo;
	}

	/*
	 * ToString
	 */
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", socSecNo=" + socSecNo + ", cityOfBirth="
				+ cityOfBirth + ", dateOfBirth=" + dateOfBirth + ", emailAddresses=" + emailAddresses
				+ ", postalAddresses=" + postalAddresses + ", phoneNumber=" + phoneNumbers + "]";
	}

}
