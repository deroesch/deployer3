package org.deroesch.toys;

import org.deroesch.helpers.Utils;

public class USPhoneNumber implements PhoneNumber {

	/*
	 * Fields
	 */
	private String areaCode = Utils.EMPTY_STRING;
	private String prefix = Utils.EMPTY_STRING;
	private String suffix = Utils.EMPTY_STRING;

	/*
	 * Constructors
	 */
	public USPhoneNumber() {
		super();
	}

	public USPhoneNumber(String areaCode, String prefix, String suffix) {
		super();
		this.setAreaCode(areaCode);
		this.setPrefix(prefix);
		this.setSuffix(suffix);
	}

	public String getAreaCode() {
		return areaCode;
	}

	/*
	 * Interface methods
	 */
	@Override
	public String getCountryCode() {
		return "1";
	}

	@Override
	public String getDisplayString() {
		return String.format("+%s (%s) %s-%s", this.getCountryCode(), this.getAreaCode(), this.getPrefix(),
				this.getSuffix());
	}

	/*
	 * Scalar fields
	 */

	public String getPrefix() {
		return prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setAreaCode(String areaCode) {
		assert null != areaCode;
		assert areaCode.length() == 3;
		this.areaCode = areaCode;
	}

	public void setPrefix(String prefix) {
		assert prefix.length() == 3;
		assert null != prefix;
		this.prefix = prefix;
	}

	public void setSuffix(String suffix) {
		assert suffix.length() == 4;
		assert null != suffix;
		this.suffix = suffix;
	}

	@Override
	public String toString() {
		return this.getDisplayString();
	}

}
